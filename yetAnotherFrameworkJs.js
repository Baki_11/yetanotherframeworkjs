(function(window) {
    const BINDING_START_SYMBOL = '{{'
    const BINDING_END_SYMBOL = '}}'

    const yaf = {
        init: init
    }

    function init(components) {
        components.forEach(component => {
            const componentInstance = new component()
            const selector = toKebabCase(componentInstance.constructor.name)
            const selectorBodyNodes = document.querySelector(selector).childNodes
            const bindings = generateBindings(selectorBodyNodes, new component())

            setInterval(() => {
                render(bindings)
            }, 100)
        })
    }

    function render(bindings) {
        bindings.forEach(binding => {
            const newValue = `${BINDING_START_SYMBOL}${binding.bindingKey}${BINDING_END_SYMBOL}`
            binding.item.innerHTML = binding.valueTrimmed.replace(newValue, binding.toValues[binding.bindingKey])
        })
    }

    function generateBindings(nodes, toValues) {
        const nodesArray = []
        nodes.forEach(item => { nodesArray.push(item) })
        return nodesArray
            .reduce((accumulator, item, index) => {
                const itemHtml = item.innerHTML
                const bindingStartIndex = itemHtml ? itemHtml.indexOf(BINDING_START_SYMBOL) : -1

                if (bindingStartIndex !== -1) {
                    const bindingEndIndex = itemHtml.indexOf(BINDING_END_SYMBOL)
                    const startSymbolLength = BINDING_START_SYMBOL.length
                    const bindingKey = getBindingNameFromString(itemHtml, bindingStartIndex, bindingEndIndex, startSymbolLength)
                    const bindingItem = {
                        bindingKey: bindingKey,
                        end: bindingEndIndex,
                        index: index,
                        item: item,
                        start: bindingStartIndex,
                        toValues: toValues,
                        valueOriginal: itemHtml,
                        valueTrimmed: trimValue(itemHtml, bindingStartIndex, bindingEndIndex, startSymbolLength, bindingKey)
                    }
                    accumulator.push(bindingItem)
                }
                return accumulator
            }, [])
    }

    function getBindingNameFromString(itemHtml, startIndex, endIndex, startSymbolLength) {
        return itemHtml.slice(startIndex + startSymbolLength, endIndex).trim()
    }

    function trimValue(itemHtml, startIndex, endIndex, startSymbolLength, bindingKey) {
        const firstPart = itemHtml.slice(0, startIndex)
        const secondPart = itemHtml.slice(endIndex + startSymbolLength)
        return `${firstPart}${BINDING_START_SYMBOL}${bindingKey}${BINDING_END_SYMBOL}${secondPart}`
    }

    function toKebabCase(input) {
        return input.replace(/([a-z])([A-Z])/g, "$1-$2")
        .replace(/\s+/g, '-')
        .toLowerCase()
    }

    if(typeof(window.yaf) === 'undefined'){
        window.yaf = yaf;
    }
})(window)
