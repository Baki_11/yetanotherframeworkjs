(function() {
    class MainPage {
        constructor(name, surname) {
            this.name = 'John'
            this.surname = 'Snow'

            setTimeout(() => {
                this.name = 'Jake'
            }, 1000)
        }
    }

    class SubPage {
        constructor(city, street, number) {
            this.city = 'Arkansas'
            this.street = 'Malibu'
            this.number = 5
        }
    }

    const yafComponents = [MainPage, SubPage]

    yaf.init(yafComponents)
})()
